# Simple Weather App

This is a simple static website that can be hosted in an S3 bucket. 
It presents a simple UI that allows the the user to enter an IP address and get the weather for that IP address. 

It gets the weather by calling our lambda functions. 

Use the script up.sh to upload the site to an s3 bucket. 

