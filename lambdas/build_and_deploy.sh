#!/usr/bin/env bash
# this program will build and deploy the weather API
# it takes three arguments
# first arg - s3 bucket name (not the url, just the name)
# second arg - ASW region e.g. us-east-1 if not supplied
# third arg  cloudformation statck name - can be any valid name, but use a self documenting name.


if [ "$#" -ne 3 ]; then
    echo "You must enter exactly two arguments s3 bucket name and the AWS region e.g. us-east-1"
    echo "build_and_deploy.sh [bucketname] [aws region] [cloudformation-stack-name"
    echo "build_and_deploy.sh mybucket us-east-1 spencer-weather-api"
    exit -1
fi

echo "***  cleanup phase\
"

rm packaged.yaml
rm -rf .aws-sam

echo "***  build and deployment phase
"

sam build
sam package --output-template packaged.yaml --s3-bucket $1
sam deploy --template-file packaged.yaml --region $2 --capabilities CAPABILITY_IAM --stack-name spencer-weather-api
