import unittest
import lambdas
from httmock import all_requests, HTTMock
from requests import Response, PreparedRequest
from unittest import mock

# we don't use context in our lambdas so this arguemnt can be safely ignored.
context = {}

# this is a hard coded event object to be consumed by the lamnbda
event = {
    "body": "eyJ0ZXN0IjoiYm9keSJ9",
    "resource": "/{proxy+}",
    "path": "/path/to/resource",
    "httpMethod": "POST",
    "isBase64Encoded": True,
    "queryStringParameters": {
        "foo": "bar"
    },
    "multiValueQueryStringParameters": {
        "foo": [
            "bar"
        ]
    },
    "pathParameters": {
        "proxy": "/path/to/resource"
    },
    "stageVariables": {
        "baz": "qux"
    },
    "headers": {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, sdch",
        "Accept-Language": "en-US,en;q=0.8",
        "Cache-Control": "max-age=0",
        "CloudFront-Forwarded-Proto": "https",
        "CloudFront-Is-Desktop-Viewer": "true",
        "CloudFront-Is-Mobile-Viewer": "false",
        "CloudFront-Is-SmartTV-Viewer": "false",
        "CloudFront-Is-Tablet-Viewer": "false",
        "CloudFront-Viewer-Country": "US",
        "Host": "1234567890.execute-api.us-east-1.amazonaws.com",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Custom User Agent String",
        "Via": "1.1 08f323deadbeefa7af34d5feb414ce27.cloudfront.net (CloudFront)",
        "X-Amz-Cf-Id": "cDehVQoZnx43VYQb9j2-nvCh-9z396Uhbp027Y2JvkCPNLmGJHqlaA==",
        "X-Forwarded-For": "127.0.0.1, 127.0.0.2",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "Accept": [
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
        ],
        "Accept-Encoding": [
            "gzip, deflate, sdch"
        ],
        "Accept-Language": [
            "en-US,en;q=0.8"
        ],
        "Cache-Control": [
            "max-age=0"
        ],
        "CloudFront-Forwarded-Proto": [
            "https"
        ],
        "CloudFront-Is-Desktop-Viewer": [
            "true"
        ],
        "CloudFront-Is-Mobile-Viewer": [
            "false"
        ],
        "CloudFront-Is-SmartTV-Viewer": [
            "false"
        ],
        "CloudFront-Is-Tablet-Viewer": [
            "false"
        ],
        "CloudFront-Viewer-Country": [
            "US"
        ],
        "Host": [
            "0123456789.execute-api.us-east-1.amazonaws.com"
        ],
        "Upgrade-Insecure-Requests": [
            "1"
        ],
        "User-Agent": [
            "Custom User Agent String"
        ],
        "Via": [
            "1.1 08f323deadbeefa7af34d5feb414ce27.cloudfront.net (CloudFront)"
        ],
        "X-Amz-Cf-Id": [
            "cDehVQoZnx43VYQb9j2-nvCh-9z396Uhbp027Y2JvkCPNLmGJHqlaA=="
        ],
        "X-Forwarded-For": [
            "127.0.0.1, 127.0.0.2"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "requestContext": {
        "accountId": "123456789012",
        "resourceId": "123456",
        "stage": "prod",
        "requestId": "c6af9ac6-7b61-11e6-9a41-93e8deadbeef",
        "requestTime": "09/Apr/2015:12:34:56 +0000",
        "requestTimeEpoch": 1428582896000,
        "identity": {
            "cognitoIdentityPoolId": None,
            "accountId": None,
            "cognitoIdentityId": None,
            "caller": None,
            "accessKey": None,
            "sourceIp": "127.0.0.1",
            "cognitoAuthenticationType": None,
            "cognitoAuthenticationProvider": None,
            "userArn": None,
            "userAgent": "Custom User Agent String",
            "user": None
        },
        "path": "/prod/path/to/resource",
        "resourcePath": "/{proxy+}",
        "httpMethod": "POST",
        "apiId": "1234567890",
        "protocol": "HTTP/1.1"
    }
}

# This is a truncated http response from the dark sky API / it is as the return value used by the mock request.get() call
good_weather_response = {"latitude": 42.360081, "longitude": -71.058884, "timezone": "America/New_York",
                         "currently": {"time": 1571941453, "summary": "Clear", "icon": "clear-day",},
                         "minutely": {"summary": "Clear for the hour.", "icon": "clear-day",
                                      "data": [
                                               {"time": 1571944980, "precipIntensity": 0, "precipProbability": 0},
                                               {"time": 1571945040, "precipIntensity": 0, "precipProbability": 0}]},
                         "hourly": {"summary": "Mostly cloudy throughout the day.", "icon": "partly-cloudy-day",
                                    "data": [{"time": 1571940000, "summary": "Clear", "icon": "clear-day",
                                              "precipIntensity": 0, "precipProbability": 0, "temperature": 67.21,
                                              "apparentTemperature": 67.21, "dewPoint": 35.87, "humidity": 0.31,
                                              "pressure": 1024.17, "windSpeed": 10.43, "windGust": 10.43,
                                              "windBearing": 221, "cloudCover": 0, "uvIndex": 3, "visibility": 10,
                                              "ozone": 258.5},

                                             {"time": 1572109200, "summary": "Partly Cloudy",
                                              "ozone": 273.2}, {"time": 1572112800, "summary": "Partly Cloudy",
                                                                "icon": "partly-cloudy-day", "precipIntensity": 0,
                                                                }]}, "daily": {
        "summary": "Rain on Saturday through Monday, with high temperatures falling to 58\u00b0F next Thursday.",
        "icon": "rain", "data": [
            {"time": 1572494400, "summary": "Mostly cloudy throughout the day.", "icon": "partly-cloudy-day",
             "apparentTemperatureMax": 57.65,
             "apparentTemperatureMaxTime": 1572523200}]}, "flags": {
        "sources": ["nwspa", "cmc", "gfs", "hrrr", "icon", "isd", "madis", "nam", "sref", "darksky", "nearest-precip"],
        "nearest-station": 0.46, "units": "us"}, "offset": -4}

# This is a truncated http response from the ipvigilante.com API / it is as the return value used by the mock request.get() call
good_get_location_reponse = {"status": "success",
                     "data": {"ipv4": "8.8.8.8", "continent_name": "North America", "country_name": "United States",
                              "city_name": "Mountain View", "latitude": "37.38600", "longitude": "-122.08380"}}


# This method will be used by the mock to replace requests.get
def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        text = "mocked_requests_get"

        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def __iter__(self):
            return self

        def __next__(self):
            return self

    return MockResponse(kwargs["data"], args[0])


class TestAPI(unittest.TestCase):

    # We patch 'requests.get' with our own method. The mock object is passed in to our test case method.
    @mock.patch('requests.get', side_effect=mocked_requests_get(200, data=good_get_location_reponse))
    def test_get_location_200(self, mock_get):
        query_parms = event["queryStringParameters"]
        query_parms["ip"] = "8.8.8.8"
        response = lambdas.get_location(event, context)
        self.assertEqual(200, response["statusCode"], "verify 200 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(400, data=good_get_location_reponse))
    def test_get_location_400(self, mock_get):
        query_parms = event["queryStringParameters"]
        query_parms["ip"] = "127.0.0.1"
        response = lambdas.get_location(event, context)
        self.assertEqual(response["statusCode"], 400, "verify 400 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(500, data=good_get_location_reponse))
    def test_get_location_500(self, mock_get):
        query_parms = event["queryStringParameters"]
        query_parms["ip"] = "127.0.0.1"
        response = lambdas.get_location(event, context)
        self.assertEqual(response["statusCode"], 500, "verify 500 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(500, data=good_get_location_reponse))
    def test_get_location_500_negative(self, mock_get):
        query_parms = event["queryStringParameters"]
        query_parms["ip"] = "127.0.0.1"
        response = lambdas.get_location(event, context)
        self.assertEqual(response["statusCode"], 500, "verify 500 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(200, data=good_weather_response))
    def test_get_weather_200(self, mock_get):
        query_parms = event["queryStringParameters"]
        query_parms["latitude"] = "42.360081"
        query_parms["longitude"] = "-71.058884"
        response = lambdas.get_weather(event, context)
        self.assertEqual(response["statusCode"], 200, "verify 200 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(400, data=good_weather_response))
    def test_get_weather_400(self, mock_get):
        query_parms = event["queryStringParameters"]
        query_parms["latitude"] = "360081"
        query_parms["longitude"] = "058884"
        response = lambdas.get_weather(event, context)
        self.assertEqual(response["statusCode"], 400, "verify 400 status code")

    def test_health_check(self):
        response = lambdas.health_check(event, context)
        self.assertTrue(response.__contains__("statusCode"), "verify health check returned something")
