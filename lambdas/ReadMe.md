## Packaging instructions 


###Step 0 make a bucket for your code.  (you only have do this once)
 Make sure that the Region for this bucket aligns with where you deployment 

aws s3 mb s3://bucketname --region region  # Example regions: us-east-1, ap-east-1, eu-central-1, sa-east-1

###Step 1

### run the build_and_deploy script 
<code>build_and_deploy.sh [bucketname] [aws region]</code>

e.g.

<code>build_and_deploy.sh mybucket us-east-1</code>

##Here's what the script does:

####Builds application
sam build

####Package the application
sam package --output-template packaged.yaml --s3-bucket *[bucketname]*

####Deploy your application
sam deploy --template-file packaged.yaml --region *[region]* --capabilities CAPABILITY_IAM --stack-name aws-sam-getting-started





